/**
 * models/payments.js
 *
 * @description :: Describes the payments functions
 * @docs        :: TODO
 */
const PaymentModel = require('../db/payments')

const Payments = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {
    const payments = await PaymentModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all payments.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      payments
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const payment = await PaymentModel
      .findOne({ _id: id })
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      payment
    }
  },

  getByStudentId: async studentId => {
    const payments = await PaymentModel
      .find({
        studentId: studentId
      })
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all payments.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      payments
    }
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {
    const payment = new PaymentModel(data)
    const result = await payment
      .save()
      .then(payment => {
        return payment
      })
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      result
    }
  }

  // getById: async id => {
  //   const params = {
  //     TableName: tableName,
  //     Key: {
  //       paymentId: {
  //         S: id
  //       }
  //     }
  //   }

  //   const getPayment = ddbClient.getItem(params).promise()
  //   const payment = await getPayment.then(payment => payment)
  //     .catch(err => {
  //       console.error('- Error trying to get a payment /:id.', JSON.stringify(err, null, 2))
  //       return {
  //         success: false,
  //         code: 500,
  //         error: JSON.stringify(err, null, 2)
  //       }
  //     })

  //   if (_.isEmpty(payment)) {
  //     return {
  //       success: false,
  //       code: 404,
  //       error: `Credit not found: ${id}`
  //     }
  //   }

  //   return {
  //     success: true,
  //     payment: Utils.removeTypesFromObject(payment.Item)
  //   }
  // },

  // getByStudentId: async studentId => {
  //   // TODO

  //   const params = {
  //     TableName: tableName,
  //     IndexName: 'StudentIndex',
  //     KeyConditionExpression: 'studentId = :studentId',
  //     ExpressionAttributeValues: {
  //       ':studentId': studentId
  //     }
  //   }

  //   const result = await Payments.findPayments(params, 'getByStudentId')

  //   return result
  // },

  // /**
  //  * create
  //  *
  //  * @description: Create a new item.
  //  * @param {object} data - List of attributes.
  //  */
  // create: async data => {
  //   const id = uuidv4()

  //   let params = {
  //     TableName: tableName,
  //     Item: {
  //       paymentId: id
  //     }
  //   }

  //   params.Item = _.extend(params.Item, data)

  //   const createLocation = docClient.put(params).promise()

  //   const result = await createLocation
  //     .then(() => {
  //       return {
  //         success: true,
  //         paymentId: id
  //       }
  //     })
  //     .catch(err => {
  //       console.error('- Error trying to create a new payment.', JSON.stringify(err, null, 2))
  //       return {
  //         success: false,
  //         code: 500,
  //         error: JSON.stringify(err, null, 2)
  //       }
  //     })

  //   return result
  // }
}

module.exports = Payments
