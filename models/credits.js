/**
 * models/credits.js
 *
 * @description :: Describes the credits functions
 * @docs        :: TODO
 */
const CreditsModel = require('../db/credits')

const Credits = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {
    const credits = await CreditsModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      credits
    }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const credit = await CreditsModel
      .findOne({ _id: id })
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      credit
    }
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {
    const credit = new CreditsModel(data)
    const result = await credit
      .save()
      .then(credit => {
        return credit
      })
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      result
    }
  }
}

module.exports = Credits
