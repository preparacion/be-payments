/**
 * router/api/credits.js
 *
 * @description :: Describes the credits api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

// const creditSchema = require('../../schemas/credits')
const creditSchema = Joi.object()
  .keys({
    amount: Joi.number(),
    date: Joi.date()
  })

const Credits = require('../../models/credits')

router.prefix('/api/credits')

// Get all items
router.get('/', async (ctx, next) => {

  const result = await Credits.get()

  if (result.success) {
    ctx.state.action = `Get all the credits`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      credits: result.credits
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Credits.getById(id)

  if (result.success) {
    ctx.state.action = `get a credit - catalog ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      credit: result.credit
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, creditSchema)

  if (validationResult.error === null) {
    const result = await Credits.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a student`
      ctx.state.data = {}

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.get('/student/:id', async (ctx, next) => {
  ctx.body = {
    success: true,
    messate: 'ok'
  }
})

module.exports = router
