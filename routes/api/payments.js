/**
 * router/api/payments.js
 *
 * @description :: Describes the payments api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Payments = require('../../models/payments')

router.prefix('/api/payments')

// Get all items
router.get('/', async (ctx, next) => {

  const result = await Payments.get()

  if (result.success) {
    ctx.state.action = `Get all the payments`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      payments: result.payments
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Payments.getById(id)

  if (result.success) {
    ctx.state.action = `get a payment - by id ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      payment: result.payment
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await Payments.create(body)

  if (result.success) {
    ctx.state.action = `create a payment`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get student payments
router.get('/student/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Payments.getByStudentId(id)

  if (result.success) {
    ctx.state.action = `get payments by student id ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
