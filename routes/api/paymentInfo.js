/**
 * router/api/paymentInfo.js
 *
 * @description :: Describes the paymentInfo api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const PaymentInfo = require('../../models/paymentInfo')

router.prefix('/api/payment_info')

// Get all items
router.get('/', async (ctx, next) => {

  const result = await PaymentInfo.get()

  if (result.success) {
    ctx.state.action = `Get all the credits`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      paymentInfos: result.paymentInfos
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await PaymentInfo.getById(id)

  if (result.success) {
    ctx.state.action = `get a paymentInfo - catalog ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      paymentInfo: result.paymentInfo
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const result = await PaymentInfo.create(body)

  if (result.success) {
    ctx.state.action = `create a paymentInfo`
    ctx.state.data = body

    ctx.status = 201
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
