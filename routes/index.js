/**
 * routes/index.js
 *
 * @description :: Defines routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const isAuth = require('../middleware/isAuth')

// Home routes
const homeApi = require('./api/home')
router.use('', homeApi.routes(), homeApi.allowedMethods())
// Payments
const paymentsApi = require('./api/payments')
router.use(isAuth, paymentsApi.routes(), paymentsApi.allowedMethods())
// Credits
const creditsApi = require('./api/credits')
router.use('', creditsApi.routes(), creditsApi.allowedMethods())
// Paymentinfo
const paymentInfoApi = require('./api/paymentInfo')
router.use('', paymentInfoApi.routes(), paymentInfoApi.allowedMethods())

module.exports = router
