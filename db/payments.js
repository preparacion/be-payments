/**
 * db/payemnts.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PaymentSchema = new Schema({
  paymentType: { type: String }, // card or cash
  amount: { type: Number, required: true },
  studentId: { type: Schema.Types.ObjectId, ref: 'Students' }
}, { versionKey: false })

// Indexes
PaymentSchema.index({ _id: 1 }, { sparse: true })
PaymentSchema.index({ studentId: 1 }, { sparse: true })

PaymentSchema.virtual('id').get(() => {
  return this._id
})

const Payment = db.model('Payment', PaymentSchema, 'Payment')

module.exports = Payment
