/**
 * db/credits.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CreditSchema = new Schema({
  amount: { type: Number },
  date: { type: Date, default: Date.now },
  studentId: { type: Schema.Types.ObjectId, ref: 'Students' }
}, { versionKey: false })

// Indexes
CreditSchema.index({ _id: 1 }, { sparse: true })
CreditSchema.index({ studentId: 1 }, { sparse: true })

CreditSchema.virtual('id').get(() => {
  return this._id
})

const Credits = db.model('Credits', CreditSchema, 'Credits')

module.exports = Credits
