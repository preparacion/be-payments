/**
 * schemas/credits.js
 *
 * @description :: Defines credits validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const creditSchema = Joi.object()
  .keys({
    amount: Joi.number(),
    date: Joi.date()
  })

module.exports = { creditSchema }
