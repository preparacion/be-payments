/**
 * middleware/isAuth.js
 *
 * @description :: Middleware for authentication
 * @docs        :: TODO
 */
const jwt = require('jsonwebtoken')

const { jwtKey } = require('../config')

module.exports = async (ctx, next) => {
  const authorization = ctx.request.header.authorization || ''

  const token = authorization.split(' ')[1] || ''
  try {
    if (!token) {
      ctx.status = 401
      ctx.body = {
        success: false,
        message: 'No auth token'
      }
      return false
    }

    const decoded = jwt.verify(token, jwtKey)

    const user = decoded.data

    ctx.state.user = JSON.parse(user)

    return next()
  } catch (err) {
    console.log(err.message)
    return next()
  }
}
