/**
 * ddb/createTables.js
 *
 * @description :: Creates tables
 * @docs        :: TODO
 */
const { createCreditsTable } = require('./credits')
const { createPaymentsTable } = require('./payments')
const { createPaymentInfoTable } = require('./paymentInfo')

async function createTables () {
  await createPaymentsTable()
  await createCreditsTable()
  await createPaymentInfoTable()
}

Promise.resolve(createTables())
  .then(() => {
    console.log('Tables created...')
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
