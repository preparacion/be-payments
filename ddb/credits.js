/**
 * ddb/credits.js
 *
 * @description :: Describes the credits db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'credits'

const creditSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'creditId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'creditId', AttributeType: 'S' },
    { AttributeName: 'studentId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'StudentIndex',
      KeySchema: [
        { AttributeName: 'studentId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createCreditsTable = async () => {
  const createTablePromise = ddbClient.createTable(creditSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Credits table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Credits table.', JSON.stringify(err, null, 2))
    })
}

const deleteCreditsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Credits table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Credits table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createCreditsTable, deleteCreditsTable }
