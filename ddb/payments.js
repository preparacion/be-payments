/**
 * ddb/payments.js
 *
 * @description :: Describes the payments db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'payments'

const paymentSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'paymentId', KeyType: 'HASH' },
  ],
  AttributeDefinitions: [
    { AttributeName: 'paymentId', AttributeType: 'S' },
    { AttributeName: 'studentId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'StudentIndex',
      KeySchema: [
        { AttributeName: 'studentId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createPaymentsTable = async () => {
  const createTablePromise = ddbClient.createTable(paymentSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ Payments table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create Payments table.', JSON.stringify(err, null, 2))
    })
}

const deletePaymentsTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ Payments table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete Payments table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createPaymentsTable, deletePaymentsTable }
