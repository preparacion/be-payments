/**
 * ddb/client.js
 *
 * @description :: Describes the locations db schema
 * @docs        :: TODO
 */
const AWS = require('aws-sdk')

const { 
  awsRegion,
  awsEndPoint,
  accessKeyId,
  secretAccessKey
} = require('../config')

// Use bluebird implementation of Promise
AWS.config.setPromisesDependency(require('bluebird'))

const updateOptions = {}
if (awsRegion) {
  updateOptions['region'] = awsRegion
}

if (awsEndPoint) {
  updateOptions['endpoint'] = awsEndPoint
}

if (accessKeyId) {
  updateOptions['accessKeyId'] = accessKeyId
}

if (secretAccessKey) {
  updateOptions['secretAccessKey'] = secretAccessKey
}

console.log(updateOptions)

AWS.config.update(updateOptions)

const ddbClient = new AWS.DynamoDB()
const docClient = new AWS.DynamoDB.DocumentClient()

module.exports = { ddbClient, docClient }
