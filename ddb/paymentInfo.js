/**
 * ddb/paymentInfo.js
 *
 * @description :: Describes the paymentInfo db schema
 * @docs        :: TODO
 */
const { ddbClient } = require('./client')

const tableName = 'paymentInfo'

const creditSchema = {
  TableName: tableName,
  KeySchema: [
    { AttributeName: 'paymentInfoId', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'paymentInfoId', AttributeType: 'S' },
    { AttributeName: 'studentId', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 20,
    WriteCapacityUnits: 20
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'StudentIndex',
      KeySchema: [
        { AttributeName: 'studentId', KeyType: 'HASH' }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    }
  ]
}

const createPaymentInfoTable = async () => {
  const createTablePromise = ddbClient.createTable(creditSchema).promise()
  await createTablePromise
    .then(result => {
      console.log('+ PaymentInfo table has been created.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to create PaymentInfo table.', JSON.stringify(err, null, 2))
    })
}

const deletePaymentInfoTable = async () => {
  const deleteTablePromise = ddbClient.deleteTable({ TableName: tableName }).promise()
  await deleteTablePromise
    .then(result => {
      console.log('+ PaymentInfo table has been deleted.', JSON.stringify(result, null, 2))
    })
    .catch(err => {
      console.error('- Error trying to delete PaymentInfo table.', JSON.stringify(err, null, 2))
    })
}

module.exports = { createPaymentInfoTable, deletePaymentInfoTable }
