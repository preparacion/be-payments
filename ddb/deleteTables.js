/**
 * ddb/createTables.js
 *
 * @description :: Creates tables
 * @docs        :: TODO
 */
const { deleteCreditsTable } = require('./credits')
const { deletePaymentsTable } = require('./payments')
const { deletePaymentInfoTable } = require('./paymentInfo')

async function deleteTables () {
  await deleteCreditsTable()
  await deletePaymentsTable()
  await deletePaymentInfoTable()
}

Promise.resolve(deleteTables())
  .then(() => {
    console.log('Tables deleted...')
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
